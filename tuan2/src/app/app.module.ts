import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { VendorTypeComponent } from './vendor-type/vendor-type.component';
import { Route } from '@angular/compiler/src/core';

//const appRoutes: Routes

@NgModule({
  declarations: [
    AppComponent,
    VendorTypeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
