import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-vendor-type',
  templateUrl: './vendor-type.component.html',
  styleUrls: ['./vendor-type.component.css']
})
export class VendorTypeComponent implements OnInit {

  arrayLNCC = [
    { id: 1, ma: 'SUA', ten: 'Nhóm nhà cung cấp nguyên liệu sữa', ghichu: 'Nhóm nhà cung cấp nguyên liệu sữa' },
    { id: 2, ma: 'DUONG', ten: 'Nhóm nhà cung cấp đường', ghichu: 'Nhóm nhà cung cấp đường' },
    { id: 3, ma: 'CN', ten: 'Nhóm nhà cung cấp thức ăn chăn nuôi', ghichu: 'Nhóm nhà cung cấp thức ăn chăn nuôi' },
    { id: 4, ma: 'DAU', ten: 'Nhóm nhà cung cấp dầu', ghichu: '' },
    { id: 5, ma: 'BOT', ten: 'Nhóm nhà cung cấp bột', ghichu: '' },
  ];

  newMaLNCC = '';
  newTenLNCC = '';
  newGhiChu = '';

  constructor() { }

  ngOnInit() {
  }

  addLNCC() {
    this.arrayLNCC.push({
      id: this.arrayLNCC.length + 1,
      ma: this.newMaLNCC,
      ten: this.newTenLNCC,
      ghichu: this.newGhiChu
    });
    this.newMaLNCC = '';
    this.newTenLNCC = '';
    this.newGhiChu = '';
  }

  removeLNCC(id: number) {
    let confirmResult = confirm("Bạn có chắc muốn xóa dữ liệu?");
    const index = this.arrayLNCC.findIndex(e => e.id == id);
    if (confirmResult) {
      this.arrayLNCC.splice(index, 1);
      // for (var i = 0; i < this.arrayLNCC.length; i++) {
      //   if (this.arrayLNCC[i].id == id)
      //     this.arrayLNCC.splice(i, 1);
      //   break;
      // }
    }
  }

}
